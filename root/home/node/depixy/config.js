const { getBool, getInt, getStr } = require("@depixy/env");

const useSSL = getBool("DEPIXY_S3_END_POINT", false);
const port = getInt("DEPIXY_S3_PORT", useSSL ? 443 : 80);

const config = {
  port: getInt("DEPIXY_PORT", 3000),
  s3: {
    endPoint: getStr("DEPIXY_S3_END_POINT"),
    useSSL,
    accessKey: getStr("DEPIXY_S3_ACCESS_KEY"),
    secretKey: getStr("DEPIXY_S3_SECRET_KEY"),
    bucket: getStr("DEPIXY_S3_BUCKET"),
    port
  }
};

module.exports = config;
