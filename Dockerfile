FROM node:lts-alpine

COPY root/ /

ENV DEPIXY_CONFIG /home/node/depixy/config.js

WORKDIR /home/node/depixy

USER node
RUN npm i

CMD ["npm", "run", "start"]
