# [depixy/image][project]

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

Docker image for Depixy image proxy.

```sh
docker run depixy/image
```

[docker]: https://hub.docker.com/r/depixy/image
[docker_pull]: https://img.shields.io/docker/pulls/depixy/image.svg
[docker_star]: https://img.shields.io/docker/stars/depixy/image.svg
[docker_size]: https://img.shields.io/microbadger/image-size/depixy/image.svg
[docker_layer]: https://img.shields.io/microbadger/layers/depixy/image.svg
[license]: https://gitlab.com/depixy/image-docker/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/depixy/image-docker/pipelines
[gitlab_ci]: https://gitlab.com/depixy/image-docker/badges/master/pipeline.svg
[project]: https://gitlab.com/depixy/image-docker
